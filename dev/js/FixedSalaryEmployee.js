(function(global) {
    function FixedSalaryEmployee(parameters) {
        this.salary = parameters.salary;
        this.name = parameters.name;
        this.id = parameters.id;
        if (parameters.firebaseUniqueId) {
            this.firebaseUniqueId = parameters.firebaseUniqueId;
        }
    }
    FixedSalaryEmployee.prototype = Object.create(Employee.prototype, {
        getSalary: {
            value: function() {
                return this.salary;
            }
        },
        constructor: {
            value: FixedSalaryEmployee
        }
    });
    global.FixedSalaryEmployee = FixedSalaryEmployee;
})(this);