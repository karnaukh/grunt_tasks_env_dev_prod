(function(global) {
    function EmployeesCollection(employees) {
        if (arguments.length) {
            this.employees = this._sort(employees);
        } else {
            this.employees = [];
        }
    }

    ////Helper to sort by DESC avg salary and ASC alphabetically in case avg salaries are equal.
    EmployeesCollection.prototype._sort = function(employees) {
        return employees.map(function(employee) {
            return new EmployeeFactory().createEmployee(employee);
        }).sort(function(a, b) {
            var x = a.name.toLowerCase();
            var y = b.name.toLowerCase();
            return (b.getSalary() - a.getSalary()) || (x < y ? -1 : x > y ? 1 : 0);
        });
    };

    EmployeesCollection.prototype.getInfo = function() {
        return this.employees.map(function(employee) {
            return {
                id: employee.id,
                name: employee.name,
                salary: employee.getSalary()
            };
        });
    };

    EmployeesCollection.prototype.getTopNames = function(quantity) {
        return this.employees.slice(0, quantity).map(function(employee) {
            return employee.name;
        });
    };

    EmployeesCollection.prototype.getLastIds = function(quantity) {
        return this.employees.slice(-quantity).map(function(employee) {
            return employee.id;
        });
    };

    EmployeesCollection.prototype.getData = function(dataType, source, callFunc) {
        switch (dataType) {
            case 'html':
                this.employees = this._sort(JSON.parse(source));
                break;
            case 'json':
                $.getJSON(source, function(data) {
                    if (Array.isArray(data)) {
                        this.employees = this._sort(data);
                    } else {
                        Object.keys(data).forEach(function(el, i) {
                            this.employees.push(data[el]);
                            this.employees[i]['firebaseUniqueId'] = el;
                        }.bind(this));
                    }
                }.bind(this)).done(callFunc);
                break;
        }
    };

    EmployeesCollection.prototype.addEmployee = function(source, key) {
        $.getJSON(source, function(data) {
            data['firebaseUniqueId'] = key;
            var emp = new EmployeeFactory().createEmployee(data);
            this.employees.push(emp);
        }.bind(this));
    };
    global.EmployeesCollection = EmployeesCollection;
})(this);