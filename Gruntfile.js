module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.repository.url %>\n' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>' +
      ' Licensed <%= pkg.license %> */\n',

    paths: {
      src: {
        js: ['dev/lib/**/*.js', 'dev/js/*.js'],
        css: ['dev/**/*.css']
      },
      dest: {
        js: 'prod/main.js',
        jsMin: 'prod/main.min.js',
        css: 'prod/main.css',
        cssMin: 'prod/main.min.css'
      }
    },
    concat: {
      options: {
        banner: '<%= banner %>',
        stripBanners: true
      },
      prod: {
        files: {
        '<%= paths.dest.js %>': '<%= paths.src.js %>',
        '<%= paths.dest.css %>': '<%= paths.src.css %>'}
      },
      dev: {}
    },
    uglify: {
      options: {
        banner: '<%= banner %>',
        mangle: false
      },
      prod: {
        src: '<%= paths.dest.js %>',
        dest: '<%= paths.dest.jsMin %>'
      },
      dev: {}
    },
    cssmin: {
      prod: {
        files: {
          '<%= paths.dest.cssMin %>': '<%= paths.dest.css %>'
        }
      },
      dev: {}
    },
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: false,
        unused: true,
        boss: true,
        eqnull: true,
        globals: {
          '$': true
        }
      },
      files: ['Gruntfile.js', 'dev/js/*.*']
    },
    watch: {
      options: {
        dateFormat: function(time) {
          var prettyTime = (function(t) {
            return [t.getHours(), t.getMinutes(), t.getSeconds()].map(function(el) {
              return String(el).length === 1 ? ('0' + el) : el;
            }).join(':');
          })(new Date());
          grunt.log.writeln(prettyTime + ' completed watch in ' + time + 'ms');
          grunt.log.writeln('Waiting for more changes in your code...');
        }
      },
      gruntfile: {
        files: 'Gruntfile.js'
      },
      src: {
        files: ['dev/**/*.*', '!lib/**/*.*'],
        tasks: ['default']
      }
    },
    clean: {
      prod: {
        files: [
          {js: ['prod/**/*.js', '!prod/**/*.min.js']},
          {css: ['prod/**/*.css', '!prod/**/*.min.css']}
        ]
      },
      dev: {},
      hooks: ['.git/hooks/pre-commit']
    },
    shell: {
      hooks: {
        command: 'cp git-hooks/pre-commit .git/hooks/'
      }
    }
  });

  var env = grunt.option('env') || 'dev';

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-git');

  grunt.registerTask('default', ['concat:' + env, 'jshint', 'uglify:' + env, 'cssmin:' + env, 'clean:' + env , 'watch' ]);
  grunt.registerTask('createhook', ['clean:hooks', 'shell:hooks']);
};